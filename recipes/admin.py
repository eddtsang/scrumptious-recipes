from django.contrib import admin
from recipes.models import Ingredient, Recipe, RecipeStep

# Register your models here.
@admin.register(Recipe)
class RecipeAdmin(admin.ModelAdmin):
    list_display = [
        "title",
        "id",
        "picture",
        "description",
        "created_on",
        "updated_on",
        "author"
    ]

@admin.register(RecipeStep)
class RecipeStep(admin.ModelAdmin):
    list_display = [
        "step_number",
        "instruction",
        "recipe"
    ]

@admin.register(Ingredient)
class Ingredient(admin.ModelAdmin):
    list_display = [
        "amount",
        "food_item",
        "recipe"
    ]
